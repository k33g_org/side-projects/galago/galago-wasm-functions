package main

import ( 
	"gitlab.com/k33g_org/side-projects/galago/galago-wasm-runner/api/function"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

func main() {}

func hey(body string) string {
	firstName := gjson.Get(body, "FirstName")
	lastName := gjson.Get(body, "LastName")

	result, _ := sjson.Set(`{"message":""}`, "message", "Hey " + firstName.Str + " " + lastName.Str)

	return result
}

//export handle
func handle(parameters *int32) *byte {
	return helpers.Use(hey, parameters)
}