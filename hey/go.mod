module gitlab.com/k33g/hey

go 1.17

require (
	github.com/tidwall/gjson v1.14.0
	gitlab.com/k33g_org/side-projects/galago/galago-wasm-runner/api/function v0.0.0-20220228193112-de2e7c7b3689
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.2.4
)
